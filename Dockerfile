FROM python:3.11-slim

RUN apt update && apt install -y gcc libpq-dev

COPY /app /app

WORKDIR /app

RUN pip install -U pip && \
    pip install poetry

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi
