from django.urls import re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import AllowAny

schema_view = get_schema_view(
    openapi.Info(
        title="Friends API",
        default_version="v1",
    ),
    public=True,
    permission_classes=[AllowAny],
    authentication_classes=[TokenAuthentication],
)


urlpatterns = [
    re_path(r"^swagger(?P<format>\.json|\.yaml)$", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    re_path(r"^swagger/$", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
]
