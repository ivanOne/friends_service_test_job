from django.contrib import admin
from django.urls import include, path

from friends import docs

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/user/", include("users.urls")),
    path("api/docs/", include(docs)),
]
