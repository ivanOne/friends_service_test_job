class RelationError(Exception):
    pass


class SendRequestError(RelationError):
    current_state = None

    def __init__(self, current_state, **kwargs):
        self.current_state = current_state

    @property
    def error_message(self):
        return ""


class DeclineRelationError(RelationError):
    pass


class ApproveRelationError(RelationError):
    pass


class DeleteFriendError(RelationError):
    pass
