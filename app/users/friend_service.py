from django.db.models import Q

from users.exceptions import (
    ApproveRelationError,
    DeclineRelationError,
    DeleteFriendError,
    RelationError,
    SendRequestError,
)
from users.models import FriendRelation


class FriendService:
    current_user_id = None

    def __init__(self, user_id):
        if not user_id:
            raise RelationError
        self.current_user_id = user_id

    def get_active_requests(self, request_type=None):
        query = FriendRelation.objects.filter(state=FriendRelation.State.AWAIT_APPROVE)
        if request_type:
            # TODO вынести в enum
            if request_type == "incoming":
                query = query.filter(friend_id=self.current_user_id)
            elif request_type == "outgoing":
                query = query.filter(user_id=self.current_user_id)
        else:
            query = query.filter(Q(user_id=self.current_user_id) | Q(friend_id=self.current_user_id))
        return query

    def get_active_friends_relations(self, include_users=False):
        qs = FriendRelation.objects.filter(Q(user_id=self.current_user_id) | Q(friend_id=self.current_user_id)).filter(
            state=FriendRelation.State.APPROVED
        )
        if include_users:
            qs = qs.select_related("user", "friend")
        return qs

    def get_relation(self, friend_user_id):
        return FriendRelation.objects.get(
            Q(friend_id=self.current_user_id, user_id=friend_user_id)
            | Q(user_id=self.current_user_id, friend_id=friend_user_id)
        )

    def send_friend_request(self, friend_user_id):
        try:
            exist_relation = self.get_relation(friend_user_id)
        except FriendRelation.DoesNotExist:
            FriendRelation.objects.create(
                user_id=self.current_user_id, friend_id=friend_user_id, state=FriendRelation.State.AWAIT_APPROVE
            )
            return

        if exist_relation.state == FriendRelation.State.DELETED:
            if friend_user_id == exist_relation.deleted_by.id:
                raise SendRequestError(current_state=FriendRelation.State.DELETED)
            else:
                exist_relation.user_id = self.current_user_id
                exist_relation.friend_id = friend_user_id
                exist_relation.state = FriendRelation.State.AWAIT_APPROVE
                exist_relation.deleted_by = None
                exist_relation.save()
                return

        elif exist_relation.get_relation_type(self.current_user_id) == "incoming":
            state_map = {
                FriendRelation.State.AWAIT_APPROVE: FriendRelation.State.APPROVED,
                FriendRelation.State.APPROVED: None,
                FriendRelation.State.DECLINED: FriendRelation.State.APPROVED,
            }
            if new_state := state_map.get(exist_relation.state, None):
                exist_relation.state = new_state
                exist_relation.save()
                return
        else:
            # Исходящие
            raise SendRequestError(current_state=exist_relation.state)

    def delete_friend(self, friend_user_id):
        try:
            relation = self.get_relation(friend_user_id)
            if relation.state == FriendRelation.State.APPROVED:
                relation.state = FriendRelation.State.DELETED
                relation.deleted_by_id = self.current_user_id
                relation.save()
                return
            else:
                raise DeleteFriendError
        except FriendRelation.DoesNotExist:
            raise DeleteFriendError

    def decline_request_friend(self, friend_user_id):
        try:
            relation = self.get_relation(friend_user_id)
            if relation.state == FriendRelation.State.AWAIT_APPROVE:
                relation.state = FriendRelation.State.DECLINED
                relation.save()
                return
            else:
                raise DeclineRelationError
        except FriendRelation.DoesNotExist:
            raise DeclineRelationError

    def approve_request_friend(self, friend_user_id):
        try:
            relation = self.get_relation(friend_user_id)
            if (
                relation.get_relation_type(self.current_user_id) == "incoming"
                and relation.state == FriendRelation.State.AWAIT_APPROVE
            ):
                relation.state = FriendRelation.State.APPROVED
                relation.save()
                return
            else:
                raise ApproveRelationError
        except FriendRelation.DoesNotExist:
            raise ApproveRelationError
