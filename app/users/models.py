from django.contrib.auth.models import User
from django.db import models


class FriendRelation(models.Model):
    class State(models.IntegerChoices):
        AWAIT_APPROVE = 0, "Заявка не одобрена"
        APPROVED = 1, "Заявка одобрена (Друзья)"
        DECLINED = 2, "Отклонена"
        DELETED = 3, "Удален из друзей"

    user = models.ForeignKey(
        User, verbose_name="Пользователь", on_delete=models.CASCADE, related_name="outgoing_relations"
    )
    friend = models.ForeignKey(User, verbose_name="Друг", on_delete=models.CASCADE, related_name="incoming_relations")
    state = models.IntegerField(verbose_name="Состояние заявки (Дружбы)")
    created_at = models.DateTimeField(verbose_name="Дата создания", auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Дата последнего обновление", auto_now=True)
    deleted_by = models.ForeignKey(
        User,
        verbose_name="Инициатор разрыва связи",
        on_delete=models.CASCADE,
        related_name="deleted_relations",
        null=True,
    )

    def get_relation_type(self, for_user_id):
        # TODO возможно стоит вынести в сервис
        if for_user_id == self.user_id:
            return "outgoing"
        elif for_user_id == self.friend_id:
            return "incoming"

    def get_friend(self, for_user_id):
        if self.user_id == for_user_id:
            return self.friend
        elif self.friend_id == for_user_id:
            return self.user

    class Meta:
        verbose_name = "Связь между пользователями"
        verbose_name_plural = "Связи между пользователями"
        constraints = [
            models.UniqueConstraint(
                fields=["user_id", "friend_id"], name="relationships between users in only one record"
            )
        ]
