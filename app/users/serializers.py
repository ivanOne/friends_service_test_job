from django.contrib.auth.models import User
from rest_framework import serializers


class UserCreateSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username"]


class FriendRequestCreateSerializer(serializers.Serializer):
    friend_id = serializers.IntegerField()


class FriendRequestSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField()
    request_type = serializers.CharField()


class FriendRequestStatusSerializer(serializers.Serializer):
    status = serializers.CharField()
