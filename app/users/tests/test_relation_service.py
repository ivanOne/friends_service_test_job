from django.contrib.auth.models import User
from django.test import TestCase

from users.exceptions import (
    ApproveRelationError,
    DeclineRelationError,
    SendRequestError,
)
from users.friend_service import FriendService
from users.models import FriendRelation


class SendRequestFriend(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")

    def test_ok(self):
        FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)

        friend_relation = FriendRelation.objects.all()

        self.assertEqual(len(friend_relation), 1)
        self.assertEqual(friend_relation[0].state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(friend_relation[0].user, self.user_1)
        self.assertEqual(friend_relation[0].friend, self.user_2)

    def test_when_friend_already_await_approve(self):
        FriendService(self.user_2.pk).send_friend_request(self.user_1.pk)
        FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)

        friend_relation = FriendRelation.objects.all()

        self.assertEqual(len(friend_relation), 1)
        self.assertEqual(friend_relation[0].state, FriendRelation.State.APPROVED.value)
        self.assertEqual(friend_relation[0].user, self.user_2)
        self.assertEqual(friend_relation[0].friend, self.user_1)

    def test_when_friend_decline_request(self):
        FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)
        FriendService(self.user_2.pk).decline_request_friend(self.user_1.pk)

        with self.assertRaises(SendRequestError) as ex:
            FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)

        self.assertEqual(ex.exception.current_state, FriendRelation.State.DECLINED)

        friend_relation = FriendRelation.objects.all()

        self.assertEqual(len(friend_relation), 1)
        self.assertEqual(friend_relation[0].state, FriendRelation.State.DECLINED.value)
        self.assertEqual(friend_relation[0].user, self.user_1)
        self.assertEqual(friend_relation[0].friend, self.user_2)

    def test_when_friend_already_approve(self):
        FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)
        FriendService(self.user_2.pk).approve_request_friend(self.user_1.pk)

        with self.assertRaises(SendRequestError) as ex:
            FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)

        self.assertEqual(ex.exception.current_state, FriendRelation.State.APPROVED)

        friend_relation = FriendRelation.objects.all()

        self.assertEqual(len(friend_relation), 1)
        self.assertEqual(friend_relation[0].state, FriendRelation.State.APPROVED.value)
        self.assertEqual(friend_relation[0].user, self.user_1)
        self.assertEqual(friend_relation[0].friend, self.user_2)

    def test_multi_state(self):
        user_3 = User.objects.create_user(username="user_3")

        FriendService(self.user_2.pk).send_friend_request(self.user_1.pk)
        FriendService(self.user_1.pk).send_friend_request(self.user_2.pk)

        FriendService(user_3.pk).send_friend_request(self.user_1.pk)
        FriendService(self.user_1.pk).decline_request_friend(user_3.pk)

        FriendService(user_3.pk).send_friend_request(self.user_2.pk)
        FriendService(self.user_2.pk).approve_request_friend(user_3.pk)

        self.assertEqual(FriendRelation.objects.count(), 3)

        self.assertTrue(
            FriendRelation.objects.filter(
                user=self.user_2, friend=self.user_1, state=FriendRelation.State.APPROVED
            ).exists()
        )

        self.assertTrue(
            FriendRelation.objects.filter(user=user_3, friend=self.user_1, state=FriendRelation.State.DECLINED).exists()
        )

        self.assertTrue(
            FriendRelation.objects.filter(user=user_3, friend=self.user_2, state=FriendRelation.State.APPROVED).exists()
        )


class RequestListTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")
        self.user_3 = User.objects.create_user(username="user_3")
        self.user_4 = User.objects.create_user(username="user_4")

        self.user_2_and_user3 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_2, state=FriendRelation.State.AWAIT_APPROVE
        )

        self.user_1_and_user4 = FriendRelation.objects.create(
            user=self.user_1, friend=self.user_4, state=FriendRelation.State.AWAIT_APPROVE
        )

        self.user_1_and_user3 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_1, state=FriendRelation.State.AWAIT_APPROVE
        )

    def test_get_all_requests(self):
        all_requests = FriendService(self.user_1.pk).get_active_requests()

        self.assertEqual(all_requests.count(), 2)

        for request in all_requests:
            self.assertEqual(request.state, FriendRelation.State.AWAIT_APPROVE)
            if request.user == self.user_1:
                self.assertEqual(request.friend, self.user_4)

            if request.user == self.user_3:
                self.assertEqual(request.friend, self.user_1)

    def test_only_incoming_requests(self):
        all_requests = FriendService(self.user_1.pk).get_active_requests(request_type="incoming")

        self.assertEqual(all_requests.count(), 1)
        request = all_requests[0]
        self.assertEqual(request.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(request.user, self.user_3)
        self.assertEqual(request.friend, self.user_1)

    def test_only_outgoing_requests(self):
        all_requests = FriendService(self.user_1.pk).get_active_requests(request_type="outgoing")

        self.assertEqual(all_requests.count(), 1)
        request = all_requests[0]
        self.assertEqual(request.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(request.user, self.user_1)
        self.assertEqual(request.friend, self.user_4)

    def test_exclude_decline_requests(self):
        service = FriendService(self.user_1.pk)
        service.decline_request_friend(self.user_4)
        all_requests = service.get_active_requests()

        self.assertEqual(all_requests.count(), 1)

        request = all_requests[0]
        self.assertEqual(request.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(request.user, self.user_3)
        self.assertEqual(request.friend, self.user_1)

    def test_exclude_approve_requests(self):
        FriendService(self.user_4.pk).approve_request_friend(self.user_1.pk)
        all_requests = FriendService(self.user_1.pk).get_active_requests()

        self.assertEqual(all_requests.count(), 1)

        request = all_requests[0]
        self.assertEqual(request.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(request.user, self.user_3)
        self.assertEqual(request.friend, self.user_1)


class ApproveRequestTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")
        self.user_3 = User.objects.create_user(username="user_3")
        self.user_4 = User.objects.create_user(username="user_4")

        self.user_2_is_friend_user_3 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_2, state=FriendRelation.State.APPROVED
        )

        self.user_1_await_approve_from_user_4 = FriendRelation.objects.create(
            user=self.user_1, friend=self.user_4, state=FriendRelation.State.AWAIT_APPROVE
        )

        self.user_3_decline_request_from_user_1 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_1, state=FriendRelation.State.DECLINED
        )

    def test_when_request_is_await_approve(self):
        FriendService(self.user_4.pk).approve_request_friend(self.user_1.pk)

        self.user_1_await_approve_from_user_4.refresh_from_db()
        self.assertEqual(self.user_1_await_approve_from_user_4.state, FriendRelation.State.APPROVED)

    def test_when_approve_for_outgoing(self):
        with self.assertRaises(ApproveRelationError):
            FriendService(self.user_1.pk).approve_request_friend(self.user_4.pk)

        self.user_1_await_approve_from_user_4.refresh_from_db()
        self.assertEqual(self.user_1_await_approve_from_user_4.state, FriendRelation.State.AWAIT_APPROVE)

    def test_when_request_is_approved(self):
        with self.assertRaises(ApproveRelationError):
            FriendService(self.user_3.pk).approve_request_friend(self.user_2.pk)

        self.user_2_is_friend_user_3.refresh_from_db()
        self.assertEqual(self.user_2_is_friend_user_3.state, FriendRelation.State.APPROVED)

    def test_when_request_is_declined(self):
        with self.assertRaises(ApproveRelationError):
            FriendService(self.user_3.pk).approve_request_friend(self.user_1.pk)

        self.user_3_decline_request_from_user_1.refresh_from_db()
        self.assertEqual(self.user_3_decline_request_from_user_1.state, FriendRelation.State.DECLINED)

    def test_when_request_not_exist(self):
        with self.assertRaises(ApproveRelationError):
            FriendService(self.user_3.pk).approve_request_friend(self.user_4.pk)

        with self.assertRaises(FriendRelation.DoesNotExist):
            FriendService(self.user_3.pk).get_relation(self.user_4.pk)


class DeclineRequestTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")
        self.user_3 = User.objects.create_user(username="user_3")
        self.user_4 = User.objects.create_user(username="user_4")

        self.user_2_is_friend_user_3 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_2, state=FriendRelation.State.APPROVED
        )

        self.user_1_await_approve_from_user_4 = FriendRelation.objects.create(
            user=self.user_1, friend=self.user_4, state=FriendRelation.State.AWAIT_APPROVE
        )

        self.user_3_decline_request_from_user_1 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_1, state=FriendRelation.State.DECLINED
        )

    def test_when_request_is_approved(self):
        with self.assertRaises(DeclineRelationError):
            FriendService(self.user_3.pk).decline_request_friend(self.user_2.pk)

        self.user_2_is_friend_user_3.refresh_from_db()
        self.assertEqual(self.user_2_is_friend_user_3.state, FriendRelation.State.APPROVED)

    def test_when_request_is_await_approve(self):
        FriendService(self.user_4.pk).decline_request_friend(self.user_1.pk)

        self.user_1_await_approve_from_user_4.refresh_from_db()
        self.assertEqual(self.user_3_decline_request_from_user_1.state, FriendRelation.State.DECLINED)

    def test_when_request_is_already_declined(self):
        with self.assertRaises(DeclineRelationError):
            FriendService(self.user_3.pk).decline_request_friend(self.user_1.pk)

        self.user_3_decline_request_from_user_1.refresh_from_db()
        self.assertEqual(self.user_3_decline_request_from_user_1.state, FriendRelation.State.DECLINED)

    def test_when_request_not_exist(self):
        with self.assertRaises(DeclineRelationError):
            FriendService(self.user_3.pk).decline_request_friend(self.user_4.pk)

        with self.assertRaises(FriendRelation.DoesNotExist):
            FriendService(self.user_3.pk).get_relation(self.user_4.pk)


class DeleteFromFriendsTestCase(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")

        self.user_1_is_friend_user_2 = FriendRelation.objects.create(
            user=self.user_1, friend=self.user_2, state=FriendRelation.State.APPROVED
        )

    def test_user_1_delete_user2(self):
        FriendService(self.user_1.pk).delete_friend(self.user_2.pk)

        self.user_1_is_friend_user_2.refresh_from_db()
        self.assertEqual(self.user_1_is_friend_user_2.state, FriendRelation.State.DELETED)
        self.assertEqual(self.user_1_is_friend_user_2.deleted_by, self.user_1)

    def test_user_1_send_request_user_2_after_delete_by_user_1(self):
        service = FriendService(self.user_1.pk)
        service.delete_friend(self.user_2.pk)
        service.send_friend_request(self.user_2.pk)

        self.user_1_is_friend_user_2.refresh_from_db()
        self.assertEqual(self.user_1_is_friend_user_2.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(self.user_1_is_friend_user_2.user, self.user_1)
        self.assertEqual(self.user_1_is_friend_user_2.friend, self.user_2)
        self.assertEqual(self.user_1_is_friend_user_2.deleted_by, None)

    def test_user_2_send_request_user_1_after_delete_by_user_1(self):
        service = FriendService(self.user_1.pk)
        service.delete_friend(self.user_2.pk)

        with self.assertRaises(SendRequestError):
            FriendService(self.user_2.pk).send_friend_request(self.user_1.pk)

        self.user_1_is_friend_user_2.refresh_from_db()
        self.assertEqual(self.user_1_is_friend_user_2.state, FriendRelation.State.DELETED)
        self.assertEqual(self.user_1_is_friend_user_2.deleted_by, self.user_1)

    def test_user_2_delete_user_1(self):
        FriendService(self.user_2.pk).delete_friend(self.user_1.pk)

        self.user_1_is_friend_user_2.refresh_from_db()
        self.assertEqual(self.user_1_is_friend_user_2.state, FriendRelation.State.DELETED)
        self.assertEqual(self.user_1_is_friend_user_2.deleted_by, self.user_2)

    def test_user_2_send_request_user_1_after_delete_by_user_2(self):
        service = FriendService(self.user_2.pk)
        service.delete_friend(self.user_1.pk)
        service.send_friend_request(self.user_1.pk)

        self.user_1_is_friend_user_2.refresh_from_db()
        self.assertEqual(self.user_1_is_friend_user_2.state, FriendRelation.State.AWAIT_APPROVE)
        self.assertEqual(self.user_1_is_friend_user_2.user, self.user_2)
        self.assertEqual(self.user_1_is_friend_user_2.friend, self.user_1)
        self.assertEqual(self.user_1_is_friend_user_2.deleted_by, None)


class TestGetFriends(TestCase):
    def setUp(self):
        self.user_1 = User.objects.create_user(username="user_1")
        self.user_2 = User.objects.create_user(username="user_2")
        self.user_3 = User.objects.create_user(username="user_3")
        self.user_4 = User.objects.create_user(username="user_4")

        self.user_2_is_friend_user_3 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_2, state=FriendRelation.State.APPROVED
        )

        self.user_1_is_friend_user_2 = FriendRelation.objects.create(
            user=self.user_2, friend=self.user_1, state=FriendRelation.State.APPROVED
        )

        self.user_1_await_approve_from_user_4 = FriendRelation.objects.create(
            user=self.user_1, friend=self.user_4, state=FriendRelation.State.AWAIT_APPROVE
        )

        self.user_3_decline_request_from_user_1 = FriendRelation.objects.create(
            user=self.user_3, friend=self.user_1, state=FriendRelation.State.DECLINED
        )

    def test_get_friends_user_2(self):
        user_pk = self.user_2.pk
        relations = FriendService(user_pk).get_active_friends_relations()

        self.assertEqual(relations.count(), 2)
        self.assertEqual(
            {rel.pk for rel in relations}, {self.user_2_is_friend_user_3.pk, self.user_1_is_friend_user_2.pk}
        )
        self.assertEqual({relation.get_friend(user_pk).pk for relation in relations}, {self.user_3.pk, self.user_1.pk})

    def test_get_friends_user_1(self):
        service = FriendService(self.user_1.pk)
        friends_relations = service.get_active_friends_relations()

        self.assertEqual(friends_relations.count(), 1)
        relation = friends_relations[0]
        self.assertEqual(relation.pk, self.user_1_is_friend_user_2.pk)
        self.assertEqual(relation.get_friend(self.user_1.pk), self.user_2)
        self.assertEqual(relation.get_friend(self.user_2.pk), self.user_1)

    def test_get_friends_user_3(self):
        friends_relations = FriendService(self.user_3.pk).get_active_friends_relations()

        self.assertEqual(friends_relations.count(), 1)
        relation = friends_relations[0]
        self.assertEqual(relation.pk, self.user_2_is_friend_user_3.pk)
        self.assertEqual(relation.get_friend(self.user_2.pk), self.user_3)
        self.assertEqual(relation.get_friend(self.user_3.pk), self.user_2)

    def test_get_friends_user_4(self):
        service = FriendService(self.user_4.pk)
        friends_relations = service.get_active_friends_relations()

        self.assertEqual(friends_relations.count(), 0)
