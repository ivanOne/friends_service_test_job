from django.urls import path
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from users.views import (
    UserFriendRequestView,
    UserFriendsView,
    UserInfoView,
    UserRegistrationView,
)

router = routers.SimpleRouter()
router.register("friends", UserFriendsView)
router.register("requests", UserFriendRequestView)

urlpatterns = [
    path("", UserInfoView.as_view()),
    path("registration/", UserRegistrationView.as_view()),
    path("auth/", obtain_auth_token),
]

urlpatterns += router.get_urls()
