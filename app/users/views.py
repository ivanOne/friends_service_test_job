from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from users.exceptions import (
    ApproveRelationError,
    DeclineRelationError,
    DeleteFriendError,
    SendRequestError,
)
from users.friend_service import FriendService
from users.models import FriendRelation
from users.serializers import (
    FriendRequestCreateSerializer,
    FriendRequestSerializer,
    FriendRequestStatusSerializer,
    UserCreateSerializer,
    UserSerializer,
)


class PrivateViews:
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]


class UserRegistrationView(CreateAPIView):
    serializer_class = UserCreateSerializer

    @swagger_auto_schema(operation_description="Регистрация нового пользователя")
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        valid_data = serializer.data
        user = User.objects.create_user(username=valid_data["username"], password=valid_data["password"])
        Token.objects.create(user=user)
        return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)


class UserInfoView(PrivateViews, APIView):
    @swagger_auto_schema(responses={200: UserSerializer()}, operation_description="Информация о пользователе")
    def get(self, request, *args, **kwargs):
        return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)


class UserFriendsView(PrivateViews, GenericViewSet):
    queryset = FriendRelation.objects.none()
    serializer_class = UserSerializer
    lookup_url_kwarg = "friend_id"

    @swagger_auto_schema(operation_description="Удалить из друзей")
    def destroy(self, request, *args, **kwargs):
        try:
            FriendService(request.user.id).delete_friend(kwargs[self.lookup_url_kwarg])
            return Response(status=status.HTTP_204_NO_CONTENT)
        except DeleteFriendError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"detail": "При удалении произошла ошибка"})

    @swagger_auto_schema(
        operation_description="Детальная информация о друге", responses={200: FriendRequestStatusSerializer()}
    )
    def retrieve(self, request, *args, **kwargs):
        try:
            relation = FriendService(self.request.user.id).get_relation(kwargs[self.lookup_url_kwarg])
            if relation.state == FriendRelation.State.APPROVED:
                friend_status = "Уже друзья"
            else:
                relation_type = relation.get_relation_type(self.request.user.id)
                friend_status = "Входящая заявка" if relation_type == "outgoing" else "Исходящая заявка"
        except FriendRelation.DoesNotExist:
            friend_status = "Ничего нет"
        return Response(data=FriendRequestStatusSerializer(data={"status": friend_status}), status=status.HTTP_200_OK)

    @swagger_auto_schema(operation_description="Список друзей")
    def list(self, request, *args, **kwargs):
        current_user_id = self.request.user.id
        qs = FriendService(current_user_id).get_active_friends_relations(include_users=True)
        serializer = self.get_serializer_class()(
            [relation.get_friend(for_user_id=current_user_id) for relation in qs], many=True
        )
        return Response(data=serializer.data, status=status.HTTP_200_OK)


@method_decorator(name="create", decorator=swagger_auto_schema(operation_description="Создать заявку"))
class UserFriendRequestView(PrivateViews, CreateModelMixin, GenericViewSet):
    queryset = FriendRelation.objects.none()
    serializer_class = FriendRequestSerializer

    def get_serializer_class(self):
        if self.action == "create":
            return FriendRequestCreateSerializer
        return FriendRequestSerializer

    def perform_create(self, serializer):
        try:
            FriendService(self.request.user.id).send_friend_request(serializer.data["friend_id"])
        except SendRequestError:
            # TODO Можно обработать state и сделать ошибку информативнее
            raise ValidationError(detail="Не удалось создать заявку в друзья")

    @swagger_auto_schema(operation_description="Список заявок")
    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()
        queryset = FriendService(self.request.user.id).get_active_requests().select_related("user", "friend")
        result = []

        for relation in queryset:
            user = relation.get_friend(self.request.user.id)
            result.append(
                {
                    "id": user.pk,
                    "username": user.username,
                    "request_type": relation.get_relation_type(self.request.user.id),
                }
            )
        return Response(data=serializer(result, many=True).data, status=status.HTTP_200_OK)

    @swagger_auto_schema(operation_description="Подтвердить дружбу", request_body=no_body)
    @action(detail=True, methods=["post"])
    def approve(self, request, pk):
        try:
            FriendService(request.user.id).approve_request_friend(pk)
            return Response(status=status.HTTP_200_OK)
        except ApproveRelationError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST, data={"detail": "При подтверждении заявки произошла ошибка"}
            )

    @swagger_auto_schema(operation_description="Отклонить дружбу", request_body=no_body)
    @action(detail=True, methods=["post"])
    def decline(self, request, pk):
        try:
            FriendService(request.user.id).decline_request_friend(pk)
            return Response(status=status.HTTP_200_OK)
        except DeclineRelationError:
            return Response(
                status=status.HTTP_400_BAD_REQUEST, data={"detail": "При отклонении заявки произошла ошибка"}
            )
